use actix_web::dev::AsyncResult;
use actix_web::middleware::session::Session;
use actix_web::*;
use futures::Future;

use helpers::{format_date, gen_context};
use models::app::*;
use models::comment::*;
use models::post::*;

pub fn create_post(
    (state, form, session, path): (State<AppState>, Form<PostForm>, Session, Path<String>),
) -> FutureResponse<HttpResponse> {
    if session.get::<String>("username").unwrap().is_some() {
        let msg = NewPostMsg {
            form: form.0,
            user_id: session.get::<i32>("id").unwrap().unwrap(),
        };

        let cat_name = path.into_inner();

        state
            .db
            .send(msg)
            .from_err()
            .and_then(move |res| match res {
                Ok(_) => Ok(HttpResponse::SeeOther()
                    .header("Location", format!("/c/{}", &cat_name))
                    .finish()),
                Err(err) => {
                    error!("{}", err);
                    Ok(HttpResponse::SeeOther()
                        .header("Location", format!("/c/{}", &cat_name))
                        .finish())
                }
            }).responder()
    } else {
        AsyncResult::from(HttpResponse::Unauthorized().finish()).responder()
    }
}

pub fn get_post(
    (state, session, path): (State<AppState>, Session, Path<(String, i32)>),
) -> FutureResponse<HttpResponse> {
    state
        .db
        .send(GetPost(path.as_ref().1))
        .from_err()
        .and_then(move |res| match res {
            Ok(post_with_comments) => {
                let mut ctx = gen_context(&session);
                ctx.insert("post", &post_with_comments.post);
                ctx.insert("post_id", &post_with_comments.post.id);
                ctx.insert("poster", &post_with_comments.poster);
                ctx.insert("comments", &post_with_comments.comments);
                ctx.insert("category", &path.0);
                ctx.insert("category_id", &path.1);

                let h = state.template.render("post.html", &ctx).map_err(|err| {
                    error::ErrorInternalServerError(format!("Template error, {}", err))
                })?;

                Ok(HttpResponse::Ok().content_type("text/html").body(h))
            }

            Err(_) => {
                let ctx = gen_context(&session);

                let h = state
                    .template
                    .render("not_found.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError("Template error"))?;

                Ok(HttpResponse::Ok().content_type("text/html").body(h))
            }
        }).responder()
}

pub fn create_comment(
    (state, session, json): (State<AppState>, Session, Json<CommentJson>),
) -> FutureResponse<HttpResponse> {
    let id = match session.get::<i32>("id").unwrap() {
        Some(id) => id,
        None => return AsyncResult::from(HttpResponse::Unauthorized().finish()).responder(),
    };

    let username = session.get::<String>("username").unwrap().unwrap();

    let msg = CreateComment {
        user_id: id,
        post_id: json.post_id,
        parent_id: json.parent_id,
        content: json.content.clone(),
    };

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| match res {
            Ok(comment) => Ok(HttpResponse::Created().json(json!({
                "content": comment.content,
                "id": comment.id,
                "username": username,
                "created_at": format_date(comment.created_at),
            }))),
            Err(err) => {
                error!("Error creating comment: {}", err);
                Ok(HttpResponse::InternalServerError().finish())
            }
        }).responder()
}
