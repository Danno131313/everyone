use actix::prelude::*;
use actix_web::*;
use diesel;
use diesel::prelude::*;
use std::time::SystemTime;

use database::*;
use helpers::format_date;
use models::category::*;
use models::post::{FormattedPost, Post};
use models::user::{SafeUser, User};

impl Message for GetCategory {
    type Result = Result<GetCategoryRes, Error>;
}

impl Message for CategoryList {
    type Result = Result<Vec<Category>, Error>;
}

impl Message for CategoryForm {
    type Result = Result<Category, Error>;
}

impl Handler<GetCategory> for DbExec {
    type Result = Result<GetCategoryRes, Error>;

    fn handle(&mut self, msg: GetCategory, _: &mut Self::Context) -> Self::Result {
        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting database connection"))?;

        let cat_name = msg.0;

        let cat: Category = {
            use schema::categories::dsl::*;
            categories
                .filter(&name.eq(&cat_name))
                .first::<Category>(conn)
                .map_err(|_| error::ErrorNotFound("Couldn't find category"))?
        };

        let posts: Vec<(FormattedPost, SafeUser)> = {
            use schema::posts::dsl::*;
            use schema::users;

            posts
                .filter(category_id.eq(cat.id))
                .inner_join(users::table)
                .load::<(Post, User)>(conn)
                .map_err(|_| error::ErrorInternalServerError("Error getting posts"))?
                .into_iter()
                .map(|(post, user)| {
                    (
                        FormattedPost {
                            id: post.id,
                            title: post.title,
                            created_at: format_date(post.created_at),
                            updated_at: format_date(post.updated_at),
                        },
                        SafeUser {
                            username: user.username,
                            email: String::new(),
                            id: user.id,
                        },
                    )
                }).collect()
        };

        Ok(GetCategoryRes {
            category: cat,
            posts,
        })
    }
}

impl Handler<CategoryList> for DbExec {
    type Result = Result<Vec<Category>, Error>;

    fn handle(&mut self, _: CategoryList, _: &mut Self::Context) -> Self::Result {
        use schema::categories::dsl::*;

        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting database connection"))?;

        let cats = categories
            .load::<Category>(conn)
            .map_err(|_| error::ErrorInternalServerError("Error getting categories"))?;

        Ok(cats)
    }
}

impl Handler<CategoryForm> for DbExec {
    type Result = Result<Category, Error>;

    fn handle(&mut self, form: CategoryForm, _: &mut Self::Context) -> Self::Result {
        use schema::categories::dsl::*;

        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting database connection"))?;

        let category = NewCategory {
            name: &form.name,
            description: &form.description,
            created_at: SystemTime::now(),
            updated_at: SystemTime::now(),
        };

        let created = diesel::insert_into(categories)
            .values(&category)
            .get_result(conn)
            .map_err(|_| {
                error::ErrorInternalServerError("Error inserting category into database")
            })?;

        Ok(created)
    }
}
