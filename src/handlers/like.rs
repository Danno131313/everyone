use actix::prelude::*;
use actix_web::*;
use diesel;
use diesel::prelude::*;

use database::*;
use models::post_like::*;

impl Message for NewPostLike {
    type Result = Result<(), Error>;
}

impl Handler<NewPostLike> for DbExec {
    type Result = Result<(), Error>;

    fn handle(&mut self, msg: NewPostLike, _: &mut Self::Context) -> Self::Result {
        use schema::post_likes::dsl::*;

        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting database connection"))?;

        diesel::insert_into(post_likes)
            .values(&msg)
            .execute(conn)
            .map_err(|_| error::ErrorInternalServerError("Error inserting post"))?;

        Ok(())
    }
}
