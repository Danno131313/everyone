use actix::prelude::*;
use actix_web::*;
use bcrypt::*;
use diesel;
use diesel::prelude::*;
use std::time::SystemTime;

use database::*;
use models::user::*;

impl Message for UserForm {
    type Result = Result<UserMsg, Error>;
}

impl Message for UserLogin {
    type Result = Result<UserMsg, Error>;
}

impl Message for UserList {
    type Result = Result<Vec<User>, Error>;
}

impl Message for GetUser {
    type Result = Result<User, Error>;
}

impl Handler<UserForm> for DbExec {
    type Result = Result<UserMsg, Error>;

    fn handle(&mut self, msg: UserForm, _: &mut Self::Context) -> Self::Result {
        use schema::users::dsl::*;

        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting database connection"))?;

        match validate_signup(&msg, conn) {
            Ok(_) => (),
            Err(errors) => return Ok(UserMsg::Error(errors)),
        }

        let hashed = hash(&msg.password, DEFAULT_COST)
            .map_err(|_| error::ErrorInternalServerError("Error hashing password"))?;

        let user = NewUser {
            username: &msg.username,
            email: &msg.email,
            password: &hashed,
            created_at: SystemTime::now(),
            updated_at: SystemTime::now(),
        };

        let created = diesel::insert_into(users)
            .values(&user)
            .get_result(conn)
            .map_err(|_| error::ErrorInternalServerError("Error inserting person"))?;

        Ok(UserMsg::Success(created))
    }
}

impl Handler<UserLogin> for DbExec {
    type Result = Result<UserMsg, Error>;

    fn handle(&mut self, msg: UserLogin, _: &mut Self::Context) -> Self::Result {
        use schema::users::dsl::*;

        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting database connection"))?;

        let result = users
            .filter(&username.eq(&msg.username))
            .load::<User>(conn)
            .map_err(|_| error::ErrorInternalServerError("Could not find user"))?
            .pop();

        match result {
            Some(user) => {
                if verify(&msg.password, &user.password)
                    .map_err(|_| error::ErrorInternalServerError("Error verifying password hash"))?
                {
                    Ok(UserMsg::Success(user))
                } else {
                    Ok(UserMsg::Error(vec!["Wrong password, try again"]))
                }
            }
            None => Ok(UserMsg::Error(vec!["No user found with that username"])),
        }
    }
}

impl Handler<UserList> for DbExec {
    type Result = Result<Vec<User>, Error>;

    fn handle(&mut self, _: UserList, _: &mut Self::Context) -> Self::Result {
        use schema::users::dsl::*;

        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting database connection"))?;

        let user_list = users
            .load::<User>(conn)
            .map_err(|_| error::ErrorInternalServerError("Error getting user list"))?;

        Ok(user_list)
    }
}

impl Handler<GetUser> for DbExec {
    type Result = Result<User, Error>;

    fn handle(&mut self, msg: GetUser, _: &mut Self::Context) -> Self::Result {
        use schema::users::dsl::*;

        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting database connection"))?;

        let user = users
            .filter(username.eq(msg.0))
            .first::<User>(conn)
            .map_err(|_| error::ErrorInternalServerError("Error getting user"))?;

        Ok(user)
    }
}

fn validate_signup(msg: &UserForm, conn: &PgConnection) -> Result<(), Vec<&'static str>> {
    use schema::users::dsl::*;

    let mut errors = Vec::new();

    if msg.username.len() < 3 {
        errors.push("Username must be more than 2 characters");
    }

    if msg.password.len() < 8 {
        errors.push("Password must be at least 8 characters");
    }

    if msg.password != msg.password_confirm {
        errors.push("Password and confirm password didn't match");
    }

    let existing_user = users
        .filter(&username.eq(&msg.username))
        .load::<User>(conn)
        .expect("Couldn't get existing user from db");

    if !existing_user.is_empty() {
        errors.push("That username is already taken");
    }

    if !errors.is_empty() {
        Err(errors)
    } else {
        Ok(())
    }
}
