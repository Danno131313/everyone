use actix_web::middleware::session::Session;
use chrono::offset::Utc;
use chrono::DateTime;
use std::time::SystemTime;
use tera;

use models::comment::{CommentWithLevel, FormattedComment};

/// Generates a tera Context with username taken from session if user is logged in
pub fn gen_context(session: &Session) -> tera::Context {
    let mut ctx = tera::Context::new();

    if let Some(username) = session.get::<String>("username").unwrap() {
        ctx.insert("username", &username);
    }

    ctx
}

/// Sets flash messages in session
pub fn set_flash(session: &Session, msgs: Vec<&'static str>) {
    session.set("flash_msgs", msgs).unwrap();
}

/// Gets flash messages from session, then clear the flahes from session
pub fn get_flash(session: &Session) -> Vec<&'static str> {
    let result = session.get::<Vec<&'static str>>("flash_msgs").unwrap();
    let msgs = match result {
        Some(msgs) => msgs,
        None => Vec::new(),
    };

    session.remove("flash_msgs");

    msgs
}

pub fn format_date(time: SystemTime) -> String {
    let datetime: DateTime<Utc> = time.into();
    datetime.format("%a, %b %m at %I:%M%p UTC").to_string()
}

pub struct CommentTree {
    pub heads: Vec<CommentNode>,
}

#[derive(Debug)]
pub struct CommentNode {
    pub comment: FormattedComment,
    pub children: Vec<Box<CommentNode>>,
}

impl CommentNode {
    pub fn new(comment: FormattedComment) -> CommentNode {
        CommentNode {
            comment,
            children: Vec::new(),
        }
    }

    pub fn add(&mut self, node: CommentNode) {
        self.children.push(Box::new(node));
    }

    pub fn find(&mut self, id: i32) -> Option<&mut CommentNode> {
        let mut found_node = None;
        for node in self.children.iter_mut() {
            if found_node.is_some() {
                return found_node;
            } else if node.comment.id == id {
                return Some(node);
            } else {
                found_node = node.find(id);
            }
        }

        found_node
    }

    pub fn into_vec(self, level: u32) -> Vec<CommentWithLevel> {
        let mut results: Vec<CommentWithLevel> = Vec::new();
        results.push(CommentWithLevel {
            comment: self.comment,
            level,
        });
        for node in self.children.into_iter() {
            results.append(&mut node.into_vec(level + 1));
        }

        results
    }
}

impl CommentTree {
    pub fn new() -> CommentTree {
        CommentTree { heads: Vec::new() }
    }

    pub fn add(&mut self, comment: FormattedComment) {
        let id = comment.parent_id;
        let new_node = CommentNode::new(comment);

        if id.is_none() {
            self.heads.push(new_node);
        } else {
            for node in self.heads.iter_mut() {
                if id.unwrap() == node.comment.id {
                    node.add(new_node);
                    break;
                } else {
                    let mut res: Option<&mut CommentNode> = node.find(id.unwrap());
                    match res {
                        None => continue,
                        Some(mut found_node) => {
                            found_node.add(new_node);
                            break;
                        }
                    };
                }
            }
        }
    }

    pub fn into_vec(self) -> Vec<CommentWithLevel> {
        let mut results: Vec<CommentWithLevel> = Vec::new();
        for node in self.heads.into_iter() {
            results.append(&mut node.into_vec(0));
        }

        results
    }
}
