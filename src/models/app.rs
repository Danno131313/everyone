use actix::prelude::*;
use tera::Tera;

use database::DbExec;

pub struct AppState {
    pub db: Addr<DbExec>,
    pub template: Tera,
}
