extern crate actix;
extern crate actix_web;
extern crate bcrypt;
extern crate chrono;
#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate env_logger;
extern crate futures;
#[macro_use]
extern crate log;
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate tera;

use actix::*;
use actix_web::middleware::session::{CookieSessionBackend, SessionStorage};
use actix_web::{fs, http::header, middleware, middleware::cors::Cors, server, App};
use diesel::prelude::PgConnection;
use diesel::r2d2::{ConnectionManager, Pool};
use dotenv::dotenv;
use std::env;

mod database;
mod handlers;
mod helpers;
mod models;
mod routes;
mod schema;

use database::DbExec;
use models::app::AppState;
use routes::auth::*;
use routes::category::*;
use routes::index::*;
use routes::like::*;
use routes::post::*;
use routes::user::*;

static PORT: u32 = 8000;

fn main() {
    ::std::env::set_var("RUST_LOG", "info,actix_web=warn");
    env_logger::init();

    let sys = actix::System::new("webapp");

    dotenv().ok();

    let session_key = env::var("SESSION_KEY").expect("No SESSION_KEY");
    let db_url = if cfg!(test) {
        env::var("TESTDB_URL").expect("TESTDB_URL must be set")
    } else {
        env::var("DATABASE_URL").expect("DATABASE_URL must be set")
    };

    let manager = ConnectionManager::<PgConnection>::new(db_url);
    let conn = Pool::builder()
        .build(manager)
        .expect("Couldn't create pool");
    let addr = SyncArbiter::start(4, move || DbExec(conn.clone()));

    let app = move || {
        App::with_state(AppState {
            db: addr.clone(),
            template: compile_templates!(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*")),
        }).middleware(middleware::Logger::default())
        .middleware(SessionStorage::new(
            CookieSessionBackend::private(&session_key.as_bytes()).secure(false),
        )).resource("/", |r| r.with_async(index))
        .resource("/register", |r| {
            r.get().f(register_page);
            r.post().with_async(create_user)
        }).resource("/login", |r| {
            r.get().f(login_page);
            r.post().with_async(login)
        }).resource("/new_category", |r| {
            r.get().f(new_category);
            r.post().with_async(create_category)
        }).resource("/logout", |r| r.get().f(logout))
        .resource("/c/{category}", |r| {
            r.get().with_async(category_page);
            r.post().with_async(create_post)
        }).resource("/u/{id}", |r| r.get().with_async(get_user))
        .resource("/c/{category}/{id}", |r| {
            r.get().with_async(get_post);
            r.post().with_async(create_comment)
        }).default_resource(|r| r.get().f(not_found))
        .configure(|app| {
            Cors::for_app(app)
                .allowed_origin("http://localhost:8000")
                .allowed_methods(vec!["GET", "POST"])
                .allowed_headers(vec![header::AUTHORIZATION, header::ACCEPT])
                .allowed_header(header::CONTENT_TYPE)
                .max_age(3600)
                .resource("/c/{category}/{id}/likes", |r| {
                    r.post().with_async(create_like)
                }).register()
        }).handler("/static/", fs::StaticFiles::new("./static").unwrap())
    };
    server::new(app)
        .bind(format!("127.0.0.1:{}", PORT))
        .unwrap()
        .start();

    info!("Running on port {}", PORT);
    sys.run();
}
