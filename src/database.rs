use actix::*;
use diesel::pg::PgConnection;
use diesel::r2d2::{ConnectionManager, Pool};

pub struct DbExec(pub Pool<ConnectionManager<PgConnection>>);

impl Actor for DbExec {
    type Context = SyncContext<Self>;
}
