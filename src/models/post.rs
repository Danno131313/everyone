use std::time::SystemTime;

use models::comment::CommentWithLevel;
use models::user::SafeUser;
use schema::posts;

#[derive(Queryable, Identifiable, Associations, PartialEq, Debug, Serialize)]
pub struct Post {
    pub id: i32,
    pub user_id: i32,
    pub category_id: i32,
    pub title: String,
    pub content: Option<String>,
    pub img: Option<String>,
    pub created_at: SystemTime,
    pub updated_at: SystemTime,
}

#[derive(Serialize)]
pub struct PostWithComments {
    pub post: Post,
    pub poster: SafeUser,
    pub comments: Vec<CommentWithLevel>,
}

#[derive(Serialize)]
pub struct FormattedPost {
    pub id: i32,
    pub title: String,
    pub created_at: String,
    pub updated_at: String,
}

#[derive(Deserialize, Debug)]
pub struct PostForm {
    pub title: String,
    pub content: String,
    pub img: String,
    pub category_id: i32,
}

#[derive(Debug)]
pub struct NewPostMsg {
    pub form: PostForm,
    pub user_id: i32,
}

#[derive(Insertable, Debug)]
#[table_name = "posts"]
pub struct NewPost<'a> {
    pub title: &'a str,
    pub content: Option<&'a str>,
    pub img: Option<&'a str>,
    pub user_id: i32,
    pub category_id: i32,
    pub created_at: SystemTime,
    pub updated_at: SystemTime,
}

pub struct GetPost(pub i32);

pub struct PostList;
