table! {
    categories (id) {
        id -> Int4,
        name -> Varchar,
        description -> Varchar,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    comments (id) {
        id -> Int4,
        user_id -> Int4,
        post_id -> Int4,
        parent_id -> Nullable<Int4>,
        content -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    post_likes (id) {
        id -> Int4,
        user_id -> Int4,
        post_id -> Int4,
        val -> Bool,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    posts (id) {
        id -> Int4,
        user_id -> Int4,
        category_id -> Int4,
        title -> Varchar,
        content -> Nullable<Text>,
        img -> Nullable<Varchar>,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        email -> Varchar,
        password -> Varchar,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

joinable!(comments -> posts (post_id));
joinable!(comments -> users (user_id));
joinable!(post_likes -> posts (post_id));
joinable!(post_likes -> users (user_id));
joinable!(posts -> categories (category_id));
joinable!(posts -> users (user_id));

allow_tables_to_appear_in_same_query!(categories, comments, post_likes, posts, users,);
