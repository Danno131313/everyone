[![pipeline status](https://gitlab.com/Danno131313/everyone/badges/master/pipeline.svg)](https://gitlab.com/Danno131313/everyone/commits/master)
# everyone

The very beginnings of a social media site inspired by Reddit and others, open-source and written in Rust with actix-web and diesel.

To get running, you must have a PostgreSQL server running and the URL provided through an environment variable (or a ```.env``` file like the example) as ```DATABASE_URL```, as well as a ```SESSION_KEY``` variable of a 32 character (minimum) string. Use ```diesel_cli``` (```cargo install diesel_cli``` to install it) to set up the database tables and run the migrations with ```diesel migration run```. Then use ```cargo run``` to compile and run on port 8000.
