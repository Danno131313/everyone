use actix_web::middleware::session::Session;
use actix_web::*;
use futures::Future;

use helpers::*;
use models::app::*;
use models::user::*;

pub fn create_user(
    (state, form, session): (State<AppState>, Form<UserForm>, Session),
) -> FutureResponse<HttpResponse> {
    state
        .db
        .send(form.0)
        .from_err()
        .and_then(move |res| match res {
            Ok(usermsg) => match usermsg {
                UserMsg::Success(user) => {
                    debug!("User {} was created", user.username);

                    session.set("username", user.username.clone()).unwrap();
                    session.set("id", user.id).unwrap();

                    Ok(HttpResponse::SeeOther().header("Location", "/").finish())
                }

                UserMsg::Error(errors) => {
                    set_flash(&session, errors);

                    Ok(HttpResponse::SeeOther()
                        .header("Location", "/register")
                        .finish())
                }
            },

            Err(_) => Ok(HttpResponse::SeeOther()
                .header("Location", "/register")
                .finish()),
        }).responder()
}

pub fn get_user(
    (state, session, path): (State<AppState>, Session, Path<String>),
) -> FutureResponse<HttpResponse> {
    state
        .db
        .send(GetUser(path.into_inner()))
        .from_err()
        .and_then(move |res| match res {
            Ok(user) => {
                let mut ctx = gen_context(&session);

                let safe_user = SafeUser {
                    id: user.id,
                    username: user.username,
                    email: user.email,
                };

                ctx.insert("user", &safe_user);

                let h = state
                    .template
                    .render("user.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError("Template error"))?;

                Ok(HttpResponse::Ok().content_type("text/html").body(h))
            }

            Err(_) => Ok(HttpResponse::NotFound().finish()),
        }).responder()
}
