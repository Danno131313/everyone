use actix_web::middleware::session::{RequestSession, Session};
use actix_web::*;
use futures::Future;

use helpers::*;
use models::app::*;
use models::user::*;

pub fn register_page(req: &HttpRequest<AppState>) -> Result<HttpResponse> {
    if req.session().get::<String>("username").unwrap().is_some() {
        return Ok(HttpResponse::TemporaryRedirect()
            .header("Location", "/")
            .finish());
    };

    let flashes = get_flash(&req.session());
    let mut ctx = gen_context(&req.session());
    ctx.insert("flashes", &flashes);

    let h = req
        .state()
        .template
        .render("register.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError("Template error"))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(h))
}

pub fn login_page(req: &HttpRequest<AppState>) -> Result<HttpResponse> {
    if req.session().get::<String>("username").unwrap().is_some() {
        return Ok(HttpResponse::TemporaryRedirect()
            .header("Location", "/")
            .finish());
    };

    let flashes = get_flash(&req.session());
    let mut ctx = gen_context(&req.session());
    ctx.insert("flashes", &flashes);

    let h = req
        .state()
        .template
        .render("login.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError("Template error"))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(h))
}

pub fn login(
    (state, form, session): (State<AppState>, Form<UserLogin>, Session),
) -> FutureResponse<HttpResponse> {
    state
        .db
        .send(form.0)
        .from_err()
        .and_then(move |res| match res {
            Ok(msg) => match msg {
                UserMsg::Success(user) => {
                    session.set("username", user.username.clone()).unwrap();
                    session.set("id", user.id).unwrap();

                    Ok(HttpResponse::SeeOther().header("Location", "/").finish())
                }

                UserMsg::Error(errors) => {
                    set_flash(&session, errors);

                    Ok(HttpResponse::SeeOther()
                        .header("Location", "/login")
                        .finish())
                }
            },

            Err(_) => Ok(HttpResponse::SeeOther().header("Location", "/").finish()),
        }).responder()
}

pub fn logout(req: &HttpRequest<AppState>) -> HttpResponse {
    req.session().clear();
    HttpResponse::TemporaryRedirect()
        .header("Location", "/")
        .finish()
}
