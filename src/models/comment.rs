use std::time::SystemTime;

use schema::comments;

#[derive(Queryable, Identifiable, PartialEq, Debug, Serialize)]
#[table_name = "comments"]
pub struct Comment {
    pub id: i32,
    pub user_id: i32,
    pub post_id: i32,
    pub parent_id: Option<i32>,
    pub content: String,
    pub created_at: SystemTime,
    pub updated_at: SystemTime,
}

#[derive(Serialize, Debug)]
pub struct FormattedComment {
    pub id: i32,
    pub content: String,
    pub parent_id: Option<i32>,
    pub poster: String,
    pub created_at: String,
    pub updated_at: String,
}

#[derive(Serialize, Debug)]
pub struct CommentWithLevel {
    pub comment: FormattedComment,
    pub level: u32,
}

#[derive(Deserialize, Serialize)]
pub struct CommentJson {
    pub post_id: i32,
    pub parent_id: Option<i32>,
    pub content: String,
}

pub struct CreateComment {
    pub user_id: i32,
    pub post_id: i32,
    pub parent_id: Option<i32>,
    pub content: String,
}

#[derive(Insertable, Debug)]
#[table_name = "comments"]
pub struct NewComment<'a> {
    pub user_id: i32,
    pub post_id: i32,
    pub parent_id: Option<i32>,
    pub content: &'a str,
    pub created_at: SystemTime,
    pub updated_at: SystemTime,
}
