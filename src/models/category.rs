use std::time::SystemTime;

use models::post::FormattedPost;
use models::user::SafeUser;
use schema::categories;

#[derive(Queryable, Identifiable, PartialEq, Debug, Serialize)]
#[table_name = "categories"]
pub struct Category {
    pub id: i32,
    pub name: String,
    pub description: String,
    pub created_at: SystemTime,
    pub updated_at: SystemTime,
}

pub struct GetCategory(pub String);

pub struct GetCategoryRes {
    pub category: Category,
    pub posts: Vec<(FormattedPost, SafeUser)>,
}

pub struct CategoryList;

#[derive(Deserialize)]
pub struct CategoryForm {
    pub name: String,
    pub description: String,
}

#[derive(Insertable, Debug)]
#[table_name = "categories"]
pub struct NewCategory<'a> {
    pub name: &'a str,
    pub description: &'a str,
    pub created_at: SystemTime,
    pub updated_at: SystemTime,
}
