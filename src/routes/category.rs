use actix_web::dev::AsyncResult;
use actix_web::middleware::session::{RequestSession, Session};
use actix_web::*;
use futures::Future;
use tera;

use helpers::gen_context;
use models::app::*;
use models::category::*;

pub fn category_page(
    (state, session, path): (State<AppState>, Session, Path<String>),
) -> FutureResponse<HttpResponse> {
    state
        .db
        .send(GetCategory(path.into_inner()))
        .from_err()
        .and_then(move |res| match res {
            Ok(cat_res) => {
                let mut ctx = gen_context(&session);
                ctx.insert("category_name", &cat_res.category.name);
                ctx.insert("posts", &cat_res.posts);
                ctx.insert("category_id", &cat_res.category.id);

                let h = state
                    .template
                    .render("category.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError("Template error"))?;

                Ok(HttpResponse::Ok().content_type("text/html").body(h))
            }

            Err(_) => {
                let ctx = gen_context(&session);

                let h = state
                    .template
                    .render("not_found.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError("Template error"))?;

                Ok(HttpResponse::Ok().content_type("text/html").body(h))
            }
        }).responder()
}

pub fn create_category(
    (state, session, form): (State<AppState>, Session, Form<CategoryForm>),
) -> FutureResponse<HttpResponse> {
    if session.get::<String>("username").unwrap().is_some() {
        state
            .db
            .send(form.0)
            .from_err()
            .and_then(|res| match res {
                Ok(_) => Ok(HttpResponse::SeeOther().header("Location", "/").finish()),
                Err(err) => {
                    error!("{}", err);
                    Ok(HttpResponse::SeeOther().header("Location", "/").finish())
                }
            }).responder()
    } else {
        AsyncResult::from(HttpResponse::Unauthorized().finish()).responder()
    }
}

pub fn new_category(req: &HttpRequest<AppState>) -> Result<HttpResponse> {
    let username: Option<String> = req.session().get::<String>("username").unwrap();

    if username.is_some() {
        let mut ctx = tera::Context::new();
        ctx.insert("username", &username.unwrap());

        let h = req
            .state()
            .template
            .render("new_category.html", &ctx)
            .map_err(|_| error::ErrorInternalServerError("Template error"))?;

        Ok(HttpResponse::Ok().content_type("text/html").body(h))
    } else {
        Ok(HttpResponse::Unauthorized().finish())
    }
}
