use std::time::SystemTime;

use schema::post_likes;

#[derive(Queryable, Identifiable, Associations, PartialEq, Debug, Serialize)]
pub struct PostLike {
    pub id: i32,
    pub user_id: i32,
    pub post_id: i32,
    pub val: bool,
    pub created_at: SystemTime,
    pub updated_at: SystemTime,
}

#[derive(Insertable, Debug)]
#[table_name = "post_likes"]
pub struct NewPostLike {
    pub user_id: i32,
    pub post_id: i32,
    pub val: bool,
    pub created_at: SystemTime,
    pub updated_at: SystemTime,
}
