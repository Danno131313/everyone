use std::time::SystemTime;

use schema::users;

pub enum UserMsg {
    Success(User),
    Error(Vec<&'static str>),
}

#[derive(Deserialize, Debug)]
pub struct UserForm {
    pub username: String,
    pub email: String,
    pub password: String,
    pub password_confirm: String,
}

#[derive(Deserialize, Debug)]
pub struct UserLogin {
    pub username: String,
    pub password: String,
}

#[derive(Queryable, Identifiable, PartialEq, Debug)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub email: String,
    pub password: String,
    pub created_at: SystemTime,
    pub updated_at: SystemTime,
}

pub struct UserList;

#[derive(Insertable, Debug)]
#[table_name = "users"]
pub struct NewUser<'a> {
    pub username: &'a str,
    pub email: &'a str,
    pub password: &'a str,
    pub created_at: SystemTime,
    pub updated_at: SystemTime,
}

#[derive(Debug, Serialize)]
pub struct SafeUser {
    pub id: i32,
    pub username: String,
    pub email: String,
}

pub struct GetUser(pub String);
