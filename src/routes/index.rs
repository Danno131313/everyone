use actix_web::middleware::session::RequestSession;
use actix_web::*;
use futures::Future;

use helpers::gen_context;
use models::app::*;
use models::category::*;

pub fn index(req: HttpRequest<AppState>) -> FutureResponse<HttpResponse> {
    req.state()
        .db
        .send(CategoryList)
        .from_err()
        .and_then(move |res| match res {
            Ok(categories) => {
                let mut ctx = gen_context(&req.session());
                ctx.insert("categories", &categories);

                let h = req
                    .state()
                    .template
                    .render("index.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError("Template error"))?;

                Ok(HttpResponse::Ok().content_type("text/html").body(h))
            }

            Err(_) => Ok(HttpResponse::InternalServerError().finish()),
        }).responder()
}

pub fn not_found(req: &HttpRequest<AppState>) -> Result<HttpResponse> {
    let ctx = gen_context(&req.session());

    let h = req
        .state()
        .template
        .render("not_found.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError("Template error"))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(h))
}
