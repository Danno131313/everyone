$(document).ready(function() {
    $(document.body).on("click", ".new_comment", function(event) {
        $(".comment_reply_form").hide();
        $(".reply_button").show();

        var post_id = event.target.dataset["postId"];
        var cat_name = event.target.dataset["categoryName"];
        var parent_id = event.target.dataset["parentId"];
        var el = $("#new_comment_content_" + parent_id);
        var content = el.val();

        if (parent_id === "0") {
            parent_id = null;
        } else {
            parent_id = parseInt(parent_id, 10);
        }

        axios.post("/c/" + cat_name + "/" + post_id, {
            post_id: parseInt(post_id, 10),
            parent_id: parent_id,
            content: content,
        }).then(function(res) {
            el.val('');
        });
    });

    $(document.body).on("click", ".reply_button", function(event) {
        console.log(event);
        var category_name = event.target.dataset["categoryName"];
        var post_id = event.target.dataset["postId"];
        var comment_id = event.target.dataset["commentId"];

        $(this).hide();
        $(this).after("<div class='ui form comment_reply_form'> \
            <div class='field'> \
            <label>Enter a comment:</label> \
            <textarea id='new_comment_content_" + comment_id + "' name='comment' rows='2'></textarea> \
            </div> \
            <button class='ui button new_comment' data-parent-id='" + comment_id + "' data-post-id='" + post_id + "' data-category-name='" + category_name + "'>Submit</button> \
            </div>");
    });
});
