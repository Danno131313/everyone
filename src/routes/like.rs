use actix_web::dev::AsyncResult;
use actix_web::middleware::session::Session;
use actix_web::*;
use futures::Future;
use std::time::SystemTime;

use models::app::*;
use models::post_like::*;

pub fn create_like(
    (state, session, path): (State<AppState>, Session, Path<(String, i32)>),
) -> FutureResponse<HttpResponse> {
    let id = match session.get::<i32>("id").unwrap() {
        Some(id) => id,
        None => return AsyncResult::from(HttpResponse::Unauthorized().finish()).responder(),
    };

    let msg = NewPostLike {
        user_id: id,
        post_id: path.into_inner().1,
        val: true,
        created_at: SystemTime::now(),
        updated_at: SystemTime::now(),
    };

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| match res {
            Ok(_) => Ok(HttpResponse::Created().finish()),
            Err(_) => Ok(HttpResponse::InternalServerError().finish()),
        }).responder()
}
