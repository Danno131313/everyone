use actix::prelude::*;
use actix_web::*;
use diesel;
use diesel::prelude::*;
use std::time::SystemTime;

use database::*;
use helpers::{format_date, CommentTree};
use models::comment::*;
use models::post::*;
use models::user::{SafeUser, User};

impl Message for PostList {
    type Result = Result<Vec<Post>, Error>;
}

impl Message for NewPostMsg {
    type Result = Result<Post, Error>;
}

impl Message for GetPost {
    type Result = Result<PostWithComments, Error>;
}

impl Message for CreateComment {
    type Result = Result<Comment, Error>;
}

impl Handler<PostList> for DbExec {
    type Result = Result<Vec<Post>, Error>;

    fn handle(&mut self, _: PostList, _: &mut Self::Context) -> Self::Result {
        use schema::posts::dsl::*;

        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting database connection"))?;

        let post_list = posts
            .load::<Post>(conn)
            .map_err(|_| error::ErrorInternalServerError("Error loading posts"))?;

        Ok(post_list)
    }
}

impl Handler<NewPostMsg> for DbExec {
    type Result = Result<Post, Error>;

    fn handle(&mut self, msg: NewPostMsg, _: &mut Self::Context) -> Self::Result {
        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting databae connection"))?;

        let id = msg.user_id;

        let content = if msg.form.content == "" {
            None
        } else {
            Some(msg.form.content.as_str())
        };

        let img = if msg.form.img == "" {
            None
        } else {
            Some(msg.form.img.as_str())
        };

        let new_post = NewPost {
            title: &msg.form.title,
            content,
            img,
            user_id: id,
            category_id: msg.form.category_id,
            created_at: SystemTime::now(),
            updated_at: SystemTime::now(),
        };

        {
            use schema::posts::dsl::*;

            let created = diesel::insert_into(posts)
                .values(&new_post)
                .get_result::<Post>(conn)
                .map_err(|_| error::ErrorInternalServerError("Error inserting post"))?;

            Ok(created)
        }
    }
}

impl Handler<GetPost> for DbExec {
    type Result = Result<PostWithComments, Error>;

    fn handle(&mut self, msg: GetPost, _: &mut Self::Context) -> Self::Result {
        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting database connection"))?;

        let post: Post = {
            use schema::posts::dsl::*;
            posts
                .find(msg.0)
                .first::<Post>(conn)
                .map_err(|_| error::ErrorInternalServerError("Error getting post"))?
        };

        let user: User = {
            use schema::users::dsl::*;
            users
                .find(post.user_id)
                .first::<User>(conn)
                .map_err(|_| error::ErrorInternalServerError("Error getting poster"))?
        };

        let safe_user = SafeUser {
            id: user.id,
            username: user.username,
            email: user.email,
        };

        let comments: Vec<FormattedComment> = {
            use schema::comments::dsl::*;
            use schema::users;
            comments
                .filter(post_id.eq(post.id))
                .inner_join(users::table)
                .load::<(Comment, User)>(conn)
                .map_err(|_| error::ErrorInternalServerError("Error getting comments"))?
                .into_iter()
                .map(|(comment, user)| FormattedComment {
                    id: comment.id,
                    content: comment.content,
                    parent_id: comment.parent_id,
                    poster: user.username,
                    created_at: format_date(comment.created_at),
                    updated_at: format_date(comment.updated_at),
                }).collect()
        };

        let mut leveled_comments = CommentTree::new();
        for comment in comments.into_iter().rev() {
            leveled_comments.add(comment);
        }

        Ok(PostWithComments {
            post,
            comments: leveled_comments.into_vec(),
            poster: safe_user,
        })
    }
}

impl Handler<CreateComment> for DbExec {
    type Result = Result<Comment, Error>;

    fn handle(&mut self, msg: CreateComment, _: &mut Self::Context) -> Self::Result {
        if msg.content.len() < 1 {
            return Err(error::ErrorBadRequest("Comment must not be empty"));
        }

        let conn = &self
            .0
            .get()
            .map_err(|_| error::ErrorInternalServerError("Error getting database connection"))?;

        let new_comment = NewComment {
            user_id: msg.user_id,
            post_id: msg.post_id,
            parent_id: msg.parent_id,
            content: &msg.content,
            created_at: SystemTime::now(),
            updated_at: SystemTime::now(),
        };

        let comment: Comment = {
            use schema::comments::dsl::*;
            diesel::insert_into(comments)
                .values(&new_comment)
                .get_result::<Comment>(conn)
                .map_err(|_| error::ErrorInternalServerError("Error creating comment"))?
        };

        Ok(comment)
    }
}
